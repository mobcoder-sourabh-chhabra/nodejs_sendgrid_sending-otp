const express = require("express");
var bodyParser = require('body-parser')
const mongoose = require("mongoose");
const app = express();
require('dotenv').config()
const PORT = process.env.PORT
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));


// mongoose.connect(
//   "mongodb://localhost/sourabhChhabra",
//   { useNewUrlParser: true, useUnifiedTopology: true },
//   function (err) {
//     if (err) {
//       console.log("DB Error: ", err);
//       process.exit(1);
//     } else {
//       console.log("MongoDB Connected");
//     }
//   }
// )

mongoose.connect("mongodb://localhost/sourabhChhabra", { useNewUrlParser: true })
  .then(() => console.log('Successfully connected to MongoDB'))
  .catch((err) => console.error(err));


// swagger integration
// const swagger = require('./swagger/swaggerSetup')
// swagger(app)

app.use("/user", require("./auth/routes/userRoutes"));

module.exports =app.listen(PORT, () => console.log(`Server is running on: ${PORT}`));
// app.listen(PORT, () => console.log(`Server is running on: ${PORT}`));