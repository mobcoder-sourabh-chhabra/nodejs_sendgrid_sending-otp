const express = require("express");
const app = express();
const router = express.Router();


const {
  userRegisters,

  getUserProfile,

  updateEmail,

  verifyOTP
} = require("../controllers/userController");

router.post("/register", userRegisters);


router.get("/getUserProfile", getUserProfile);

router.put('/updateEmail',updateEmail)

router.post('/verifyOTP',verifyOTP)

module.exports = router;

