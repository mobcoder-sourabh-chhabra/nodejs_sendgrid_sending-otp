const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Userss = require("../models/userModel");

 

const sgMail = require('@sendgrid/mail')
const sgMailApiKey = ''
sgMail.setApiKey(sgMailApiKey)




exports.userRegisters = async (req, res) => {
  userEmail= req.body.userEmail;
  const user = await Userss.find({ userEmail: userEmail })

if(user.length==0) {

  const otp = Math.floor(1000 + Math.random() * 9000); // Generate a random 4 digit OTP
  const msg = {
    to: req.body.userEmail,
    from: 'sourabh.chhabra@mobcoder.com',
    subject: 'OTP for your account',
    text: `Your OTP is: ${otp}`,
  };
  sgMail.send(msg) // Use the SendGrid API to send the OTP
  .then((result) => {
    const newUser = new Userss({
      userEmail: req.body.userEmail,
      OTP: otp,
      msg: "OTP SENT SUCCESSFULLY"
    });
    newUser.save()
    .then((result) => {
      res.status(200).json({
      
        msg: "Save Successfully"
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
  })
  .catch((err) => {
    console.log(err);
    res.status(500).json({
      error: err,
    });
  });
}
else{
  if(user[0].userVerifies=='false'){
    const otp = Math.floor(1000 + Math.random() * 9000); // Generate a random 4 digit OTP
  const msg = {
    to: req.body.userEmail,
    from: 'sourabh.chhabra@mobcoder.com',
    subject: 'OTP for your account',
    text: `Your OTP is: ${otp}`,
  };
  sgMail.send(msg) 
    // res.status(200).json({
    //   msg: "Your Email Not Verified OTP is send To Mail Please Verifies IT"
    // })
    .then((result) => {
      Userss.findOneAndUpdate({userEmail: req.body.userEmail}, {OTP: otp}) // Use the Mongoose findOneAndUpdate() method to save the OTP to the user model
      .then((result) => {
        res.status(200).json({
          msg: "Your Email Not Verified hence,OTP is send To Mail Please Verifies IT"
        });
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json({
          error: err,
        });
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
  }
  else{
    res.status(200).json({
      msg: "User Is Registered Successfully, Please Enter Another Mail To Continue"
    });
  }
}

};



exports.getUserProfile = (req, res, next) => {
  const userId = req.userData.userId;

  Userss.findById({ _id: userId })
    .exec()
    .then((result) => {
      res.status(200).json({
        Data: result,
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
};


exports.updateEmail = (req, res) => {
  const userId = req.userData.userId;

  Userss.findOneAndUpdate(
    { _id: userId },
    {
      $set: {
        userEmail: req.body.userEmail,
      },
    }
  )
    .then((result) => {
      res.status(200).json({
        Data: result,
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
};





exports.verifyOTP=async(req,res)=>{
  let user = await Userss.findOne({userEmail:req.body.userEmail})
  if(req.body.OTP == user.OTP){
    Userss.findOneAndUpdate({userEmail:req.body.userEmail},{userVerifies:"true"})
    .exec()
    .then((result)=>{
      res.status(200).json({
        msg:"OTP VERIFIED SUCCESSFULLY"
      });
    })
    .catch((err)=>{
      console.log(err);
      res.status(500).json({
        error:err
      });
    });
  }
  else{
    res.status(500).json({
      msg:"OTP IS WRONG"
    });
  }
}
