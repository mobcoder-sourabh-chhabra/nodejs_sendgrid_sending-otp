const mongoose = require('mongoose')


const UserSchema = new mongoose.Schema({
   
    userId: {
        type: mongoose.Types.ObjectId},
    userEmail: {type: String, required: true, unique: true},
    
    userVerifies:{type: String, default:"false"},
    OTP: {type: String,default:0}
})

const model = mongoose.model('user',UserSchema)

module.exports = model;